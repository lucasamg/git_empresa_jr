Houve o seguinte conflito: "Auto-merging gitarquivo.txt
CONFLICT (content): Merge conflict in gitarquivo.txt
Automatic merge failed; fix conflicts and then commit the result."

Uma vez que há alterações que não se complementam na mesma linha e com branchs
diferentes, haverá conflito. Para resolver esse problema, deverá ser criado
um novo commit com o arquivo preparado para adequar os 2 códigos, uma vez que o
merge não consegue gerenciar o conflito.
